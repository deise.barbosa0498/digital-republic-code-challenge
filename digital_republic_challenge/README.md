# Digital Republic - Code Challenge

O desafio propõe a criação de uma calculadora que informa ao usuário a quantidade de tintas necessárias para pintar uma sala baseado no tamanho das paredes e quantidade de portas e janelas informadas pelo mesmo.

## Autora

- Deise Claudino Almeida Pereira Barbosa

## Tecnologias

- PHP 8.0.26
- Laravel 10.0.3

## Para executar o projeto

- Clonar projeto:

```
git clone https://gitlab.com/deise.barbosa0498/digital-republic-code-challenge.git

```

- Baixar dependências PHP:

```
composer install
```

- Renomear o arquivo .env.example >> .env, adicionar as credenciais e gerar a chave do projeto: 

```
php artisan key:generate 
```

- Rodar o projeto localmente (rodando na porta 8000):

```
php artisan serve
```