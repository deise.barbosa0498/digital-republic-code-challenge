@extends('layout_base')
@section('content')
<div class="div-bg">
    <div class="bg"></div>
    <h1>Digital Republic - Calculadora de tintas</h1>
    <p>Para realizar o cálculo de quantidade de tintas, informe a altura e largura das paredes que possui e a quantidade de portas e janelas existentes no cômodo.</p>

    @if ($message = Session::get('error'))
        <div class="warning-area">
            <div class="alert alert-danger">
            <strong>
                {{ $message }}
            </strong>
            </div>
        </div>
    @endif
    @if (isset($errors) && $errors->any())
        @foreach($errors->all() as $error)
            <div class="warning-area">
                <div class="alert alert-danger">
                    <strong>
                    {{$error}}
                    </strong>
                </div>
            </div>
        @endforeach
    @endif
    <form method="POST" action="{{route('calculate')}}">
        @csrf
        @for($i = 0; $i <= 3; $i++)
        @php
            $id_wall = $i;
        @endphp
            <h3>Parede {{ ++$id_wall }}</h3>
            <div class="row">
                <div class="column">
                    <label for="measures_all_walls[{{$i}}]['height']">Altura (m)</label>
                    <input type="numeric" id="measures_all_walls[{{$i}}]['height']" name="measures_all_walls[{{$i}}][height]" min="0" value="0">
                </div>
                <div class="column">
                    <label for="measures_all_walls[{{$i}}]['width']">Largura (m)</label>
                    <input type="numeric" id="measures_all_walls[{{$i}}]['width']" name="measures_all_walls[{{$i}}][width]" min="0" value="0">
                </div>
                <div class="column">
                    <label for="measures_all_walls[{{$i}}]['qtt_windows']">Total de Janelas</label>
                    <input type="numeric" id="measures_all_walls[{{$i}}]['qtt_windows']" name="measures_all_walls[{{$i}}][qtt_windows]" min="0" value="0">
                </div>
                <div class="column">
                    <label for="measures_all_walls[{{$i}}]['qtt_doors']">Total de Portas</label>
                    <input type="numeric" id="measures_all_walls[{{$i}}]['qtt_doors']" name="measures_all_walls[{{$i}}][qtt_doors]" min="0" value="0">
                </div>
            </div>
        @endfor
        <div>
            <button type="submit" class="button">Enviar</button>
        </div>
    </form>
@endsection
