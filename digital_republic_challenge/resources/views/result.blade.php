@extends('layout_base')
@section('content')
<div class="div-bg">
    <div class="bg"></div>
    <h1>Digital Republic - Calculadora de tintas</h1>
    <p>Confira abaixo o total de latas de tintas necessárias para pintar o cômodo informado.</p>

    <div class="container">
        @if($result["18"] > 0)
            <p><b>Total de latas de tinta de 18L:</b> {{ $result["18"] }}</p>
        @endif
        
        @if($result["3.6"] > 0)
            <p><b>Total de latas de tinta de 3,6L:</b> {{ $result["3.6"] }}</p>
        @endif
        @if($result["2.5"] > 0)
            <p><b>Total de latas de tinta de 2,5L:</b> {{ $result["2.5"] }}</p>
        @endif
        @if($result["0.5"] > 0)
            <p><b>Total de latas de tinta de 0,5L:</b> {{ $result["0.5"] }}</p>
        @endif

        <div>
            <form method="GET" action="{{route('index')}}">
                <button type="submit" class="button">Voltar</button>
            </form>
        </div>
        
    </div>
</div>
@endsection