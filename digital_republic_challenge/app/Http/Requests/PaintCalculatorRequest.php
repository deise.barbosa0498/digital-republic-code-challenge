<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaintCalculatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'measures_all_walls.*.width'         =>  'required|numeric',
            'measures_all_walls.*.height'        =>  'required|numeric',
            'measures_all_walls.*.qtt_windows'   =>  'required|numeric',
            'measures_all_walls.*.qtt_doors'     =>  'required|numeric'
        ];
    }
}
