<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PaintCalculatorRequest;

class PaintCalculatorController extends Controller
{
    public function paintCalculator(PaintCalculatorRequest $request)
    {

        $paint_per_liter = 5;
        $paint_cans = [18, 3.6, 2.5, 0.5];
        $result = [];

        $area_to_painting = $this->totalAreaFreeCalculator($request->measures_all_walls);

        if($area_to_painting['http_status'] == 412)
        {
            $request->session()->flash('error', $area_to_painting['data']);

            return redirect()->back();
        }

        $liters_paint = $area_to_painting['data'] / $paint_per_liter;

        foreach($paint_cans as $paint_can)
        {
            $total_can = 0;
            if($liters_paint > 0)
            {
                $total_can = intval(($liters_paint / $paint_can));            
                $liters_paint = fmod($liters_paint, $paint_can);

                if($paint_can == 0.5)
                {
                    $liters_paint = fmod($liters_paint, 0.5) > 0 ? $total_can++ : $total_can; 
                }
            }

            $result["$paint_can"] = $total_can;
        }

        return view('result', compact('result'));
    }

    public function areaCalculator($width, $height)
    {
        $area = ($width * $height);

        return $area;
    }

    public function totalAreaFreeCalculator($measures_all_walls)
    {
        $door_width = 0.80;
        $door_height = 1.90;
        $window_width = 2;
        $window_height= 1.20;
        $area_to_painting = 0;
        $min_height_wall_with_door = $door_height + 0.30;

        $window_area = $this->areaCalculator($window_width, $window_height);            
        $door_area   = $this->areaCalculator($door_width, $door_height);

        foreach($measures_all_walls as $key => $measures)
        {
            $id_wall    = ++$key;
            $wall_area  = $this->areaCalculator($measures['width'], $measures['height']);

            if($wall_area < 1)
            {
                $data = [
                    'http_status'   =>  412,
                    'data'          =>  "Parede " . $id_wall . ": As medidas informadas não respeitam o limite mínimo de 1m2."
                ];

                return $data;
            }

            if($wall_area > 50)
            {
                $data = [
                    'http_status'   =>  412,
                    'data'          =>  "Parede " . $id_wall . ": As medidas informadas não respeitam o limite máximo de 50m2."
                ];

                return $data;
            }

            if(($measures['qtt_doors'] > 0 && $measures['width'] < $min_height_wall_with_door))
            {
                $data = [
                    'http_status'   =>  412,
                    'data'          =>  "Parede " . $id_wall . ": Paredes com portas devem tem a altura mínima de " . $min_height_wall_with_door . '.'
                ];

                return $data;
            }   

            $total_window_door_area = ($measures['qtt_windows'] * $window_area) + ($measures['qtt_doors'] * $door_area);
            if(((100 * $total_window_door_area) / $wall_area) > 50)
            {
                $data = [
                    'http_status'   =>  412,
                    'data'          =>   "Parede " . $id_wall . ": A área total de portas e janelas ocupa mais de 50% da área da parede informada."
                ];

                return $data;
            }  
            
            //  Total Free Area for Painting
            $area_to_painting += ($wall_area - $total_window_door_area);

        }

        $data = [
            'http_status'   =>  200,
            'data'          =>  $area_to_painting
        ];

        return $data;
    }
}
